<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
});

Route::get('/history', function () {
    return view('history');
});

Route::get('/projects', [\App\Http\Controllers\ProjectController::class, 'index']);

Route::group(['prefix' => '/admin', 'middleware' => ['web']], static function (): void {
    Route::group(['middleware' => ['auth']], static function (): void {
        Route::get('/', \App\Http\Controllers\Admin\IndexController::class);
        Route::get('/logout', [\App\Http\Controllers\Admin\AuthController::class, 'destroy'])->name('logout');
        Route::get('/projects', [\App\Http\Controllers\Admin\IndexController::class, 'projects']);
        Route::get('/arts', [\App\Http\Controllers\Admin\IndexController::class, 'arts']);
        Route::get('/customers', [\App\Http\Controllers\Admin\IndexController::class, 'customers']);
    });

    Route::post('/projects/add', [\App\Http\Controllers\Admin\IndexController::class, 'createProject']);
    Route::get('/projects/{id:int}/delete', [\App\Http\Controllers\Admin\IndexController::class, 'deleteProject']);

    Route::get('/api/v1/projects', [\App\Http\Controllers\Admin\IndexController::class, 'getProjectsForApiTables']);

    Route::group(['middleware' => ['guest']], static function (): void {
        Route::get('/login', [\App\Http\Controllers\Admin\AuthController::class, 'create'])->name('login');
        Route::post('/login', [\App\Http\Controllers\Admin\AuthController::class, 'store']);
    });
});
