@extends('layout')

@push('title')Проекты | Архитектурная мастерская Образ Города@endpush
@push('description')Проектируем сооружения от общественных зданий до частных домов@endpush

@section('content')
<div class="projects">
    @foreach($projects as $project)
        <div class="project">
            <div class="project-img" style="background-image: url('{{ $project['preview_path'] }}/{{ $project['preview_name'] }}')"></div>
        </div>
    @endforeach
</div>
@endsection
