<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Образ Города</title>
    <link rel="stylesheet" type="text/css" href="/admin-assets/lib/perfect-scrollbar/css/perfect-scrollbar.css"/>
    <link rel="stylesheet" type="text/css" href="/admin-assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" type="text/css" href="/admin-assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="/admin-assets/lib/summernote/summernote-bs4.css"/>
    <link rel="stylesheet" type="text/css" href="/admin-assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/admin-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="/admin-assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" href="/admin-assets/css/app.css" type="text/css"/>
</head>
<body>
<style>
    @media(min-width: 768px) {
        .main-content {
            padding-top: 0;
        }
    }
</style>
{{--<script src="/admin-assets/lib/jquery/jquery.min.js" type="text/javascript"></script>--}}
{{--<script src="/admin-assets/lib/perfect-scrollbar/js/perfect-scrollbar.min.js" type="text/javascript"></script>--}}
{{--<script src="/admin-assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>--}}
{{--<script src="/admin-assets/js/app.js" type="text/javascript"></script>--}}
{{--<script type="text/javascript">--}}
{{--    $(document).ready(function(){--}}
{{--        App.init();--}}
{{--    });--}}
{{--</script>--}}
<div class="be-wrapper be-fixed-sidebar">
    <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
            <div class="be-navbar-header">
                <a class="navbar-brand" href="/"></a>
            </div>
            <div class="be-right-navbar">
                <ul class="nav navbar-nav float-right be-user-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
                            <img src="/assets/user.png" alt="Avatar">
                            <span class="user-name">{{ auth()->user()->email }}</span>
                        </a>
                        <div class="dropdown-menu" role="menu">
                            <div class="user-info">
                                <div class="user-name">{{ auth()->user()->email }}</div>
                            </div>
                            <a class="dropdown-item" href="/admin/logout"><span class="icon mdi mdi-power"></span>Выйти</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="be-left-sidebar">
        <div class="left-sidebar-wrapper"><a class="left-sidebar-toggle" href="#">Меню</a>
            <div class="left-sidebar-spacer">
                <div class="left-sidebar-scroll">
                    <div class="left-sidebar-content">
                        <ul class="sidebar-elements">
                            <li @if(request()->path() === 'admin') class="active" @endif>
                                <a href="/admin"><i class="icon mdi mdi-chart-donut"></i><span>Аналитика</span></a>
                            </li>
                            <li @if(request()->path() === 'admin/projects') class="active" @endif>
                                <a href="/admin/projects"><i class="icon mdi mdi-city-alt"></i><span>Проекты</span></a>
                            </li>
                            <li @if(request()->path() === 'admin/customers') class="active" @endif>
                                <a href="/admin/customers"><i class="icon mdi mdi-accounts"></i><span>Клиенты</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="be-content">
        @if(isset($notification))
            <div class="alert alert-{{ $notification->type === 'error' ? 'danger' : 'success' }} alert-dismissible" role="alert">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                <div class="icon"> <span class="mdi mdi-{{ $notification->type === 'error' ? 'close-circle-o' : 'check'}}"></span></div>
                <div class="message">{{ $notification->message }}</div>
            </div>
        @endif

        @yield('content')
    </div>
</div>
</body>
</html>
