@extends('admin.layout')

@section('content')
<div class="page-head">
    <h2 class="page-head-title">Проекты</h2>
</div>
<div class="main-content container-fluid">
    <button class="btn btn-space btn-danger" data-toggle="modal" data-target="#form-bp1" type="button" style="margin-bottom: 14px">
        <i class="icon icon-left mdi mdi-plus"></i> Добавить
    </button>

    <div class="card card-table">
        <div class="card-body">
            <table class="table table-striped table-hover" id="table1">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Категория</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                <!--Ajax content goes here-->
                </tbody>
            </table>
        </div>
    </div>

</div>

<div class="modal fade" id="form-bp1" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Добавление проекта</h3>
                <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true">
                    <span class="mdi mdi-close"></span>
                </button>
            </div>
            <div class="modal-body">
{{--                <div class="card-subtitle mb-4">--}}
{{--                    Изначально проект не будет виден на сайте.--}}
{{--                    <br/>--}}
{{--                    После того как вы закончите его редактирование вы сами сможете сделать его видимым.--}}
{{--                </div>--}}
                <form id="add-case-form" action="/admin/projects/add" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="inputTitle">Название проекта</label>
                        <input class="form-control" id="inputTitle" type="text" name="title" required>
                    </div>
                    <div class="form-group">
                        <label for="inputCategory">Категория</label>
                        <input class="form-control" id="inputCategory" type="text" name="category" required>
                    </div>
                    <div class="form-group">
                        <label for="editor">Описание</label>
                        <textarea id="editor" class="editor1"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="main-image-input">Превью</label>
                        <div>
                            <input class="inputfile" id="main-image-input" type="file" name="preview" accept="image/png, image/gif, image/jpeg, image/svg+xml" required>
                            <label class="btn-secondary" for="main-image-input">
                                <i class="mdi mdi-folder-outline"></i><span>Выбрать изображение</span>
                            </label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="padding-top: 0">
                <button class="btn btn-danger btn-md" type="submit" form="add-case-form">Продолжить</button>
            </div>
        </div>
    </div>
</div>

<script src="/admin-assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/perfect-scrollbar/js/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="/admin-assets/js/app.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/jszip/jszip.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/pdfmake/pdfmake.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/pdfmake/vfs_fonts.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/datatables.net-responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/datatables/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js" type="text/javascript"></script>
<script src="/admin-assets/js/app-form-elements.js" type="text/javascript"></script>
<script src="/admin-assets/js/app-tables-datatables.js" type="text/javascript"></script>
<script src="/admin-assets/lib/summernote/summernote-bs4.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/summernote/summernote-ext-beagle.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //-initialize the javascript
        App.init();

        $( '.inputfile' ).each( function(){
            var $input   = $( this ),
                $label   = $input.next( 'label' ),
                labelVal = $label.html();

            $input.on( 'change', function( e )
            {
                var fileName = '';

                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else if( e.target.value )
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    $label.find( 'span' ).html( fileName );
                else
                    $label.html( labelVal );
            });
        });

        $('.editor1').summernote({
            dialogsInBody: true,
            height: 140,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline']],
                ['para', ['ul', 'ol', 'paragraph']],
            ],
            styleTags: [
                'p',
                { title: 'Blockquote', tag: 'blockquote', className: 'blockquote', value: 'blockquote' },
                'pre', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
            ],
            callbacks: {
                onInit: () => $('.note-editable').find('*').css('font-size', '').css('color', '').css('background-color', ''),
                onChange: () => $('.note-editable').find('*').css('font-size', '').css('color', '').css('background-color', ''),
            }
        });

        App.dataTables({
            'processing': false,
            'serverSide': true,
            'info': false,
            'searching': false,
            'ordering': false,
            'lengthChange': false,
            'ajax': {
                'url': '/admin/api/v1/projects',
                'dataSrc': function (json) {
                    for (let i = 0, ien = json.data.length; i < ien; i++) {
                        json.data[i].preview = '<img src="' + json.data[i].image_path + '/'
                            + json.data[i].image_name + '" alt="Image"><span>' + json.data[i].title + '</span>';

                        json.data[i].action = '<a href="/admin/projects/' + json.data[i].id + '/delete" class="btn btn-space btn-secondary"><i class="icon icon-left mdi mdi-delete"></i> Удалить</a>';
                    }

                    return json.data;
                }
            },
            'columns': [
                {'data': 'id'},
                {'data': 'preview'},
                {'data': 'category'},
                {'data': 'action'},
            ],
            'language': {
                'paginate': {
                    'previous': 'Назад',
                    'next': 'Далее',
                },
            },
            'drawCallback': (settings) => {
                $('.be-datatable-header').remove();
                $('#table1 tbody td:nth-child(2n)').addClass('user-avatar cell-detail user-info');
            },
        });
    });
</script>
@endsection
