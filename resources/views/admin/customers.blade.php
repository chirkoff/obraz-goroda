@extends('admin.layout')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Клиенты</h2>
    </div>
    <div class="main-content container-fluid">
        Раздел в разработке
    </div>
    <script src="/admin-assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="/admin-assets/lib/perfect-scrollbar/js/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="/admin-assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="/admin-assets/js/app.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            App.init();
        });
    </script>
@endsection
