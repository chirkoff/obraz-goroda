<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/admin-assets/img/logo-fav.png">
    <title>Образ Города - Панель управления</title>
    <link rel="stylesheet" type="text/css" href="/admin-assets/lib/perfect-scrollbar/css/perfect-scrollbar.css"/>
    <link rel="stylesheet" type="text/css" href="/admin-assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" href="/admin-assets/css/app.css" type="text/css"/>
</head>
<body class="be-splash-screen">
<div class="be-wrapper be-login">
    <div class="be-content">
        <div class="main-content container-fluid">
            <div class="splash-container">
                <div class="card card-border-color card-border-color-danger">
                    <div class="card-header">
                        <img class="logo-img" src="/assets/logo.png" alt="logo" height="58">
                        <div style="font-family: Georgia, serif;
    font-weight: bold;
    text-transform: uppercase;">Образ Города</div>
                        <span class="splash-description">Пожалуйста заполните поля для входа.</span>
                    </div>
                    <div class="card-body">
                        @if($errors->any())
                        <div class="alert alert-contrast alert-danger alert-dismissible col-12" role="alert">
                            <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                            <div class="message">
                                {{ $errors->first() }}
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                            </div>
                        </div>
                        @endif
                        <form action="/admin/login" method="post">
                            @csrf
                            <div class="form-group">
                                <input class="form-control" name="email" placeholder="E-mail" type="email" required />
                            </div>
                            <div class="form-group">
                                <input class="form-control" name="password" type="password" placeholder="Пароль" required />
                            </div>
{{--                            <div class="form-group row login-tools">--}}
{{--                                <div class="col-6 login-remember">--}}
{{--                                    <label class="custom-control custom-checkbox">--}}
{{--                                        <input class="custom-control-input" type="checkbox">--}}
{{--                                        <span class="custom-control-label">Запомнить меня</span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="form-group login-submit">
                                <button class="btn btn-danger btn-xl" data-dismiss="modal">Войти</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/admin-assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/perfect-scrollbar/js/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="/admin-assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="/admin-assets/js/app.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        App.init();
    });
</script>
</body>
</html>
