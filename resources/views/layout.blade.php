<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>@stack('title')</title>

    <meta data-n-head="ssr" charset="utf-8">
    <meta data-n-head="ssr" data-hid="viewport" name="viewport" content="width=device-width, initial-scale=1">

    <meta data-n-head="ssr" data-hid="description" name="description" content="@stack('description')">
    <meta data-n-head="ssr" data-hid="keywords" name="keywords" content="">

    <meta data-n-head="ssr" data-hid="og:title" name="og:title" content="Архитектурная мастерская Образ Города">
    <meta data-n-head="ssr" data-hid="og:description" name="og:description" content="@stack('description')">
    <meta data-n-head="ssr" data-hid="og:type" property="og:type" content="website">
    <meta data-n-head="ssr" data-hid="og:url" property="og:url" content="{{ request()->url() }}">
    <meta data-n-head="ssr" data-hid="og:locale" property="og:locale" content="ru_RU">
    <meta data-n-head="ssr" data-hid="og:site_name" property="og:site_name" content="ObrazGoroda">

    <meta name="theme-color" content="#000">
    <meta name="msapplication-navbutton-color" content="#000">
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">

    <link rel="stylesheet" href="{{ asset('/assets/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/icons.css') }}">

    <link data-n-head="ssr" rel="canonical" href="{{ request()->url() }}">

    <style>html{--tt-base: 20;--tt-scale: 1;--tt-ease:linear;--tt-max:1600}*,:before,:after,html{--tt-key:none;animation:var(--tt-key) 1s var(--tt-ease) 1 calc(-1s * var(--tt-bind) / var(--tt-max)) both paused}</style>
</head>
<body>
<div class="header">
    <img class="logo" src="/assets/logo.png" />
    <div class="header-title">Образ Города</div>
    <div class="header-title" style="font-size: 7.6px;line-height: 10px;">Архитектурная Мастерская</div>
    <div class="menu">
        <a class="menu-link @if(request()->path() === '/') active @endif" href="/">Главная</a>
        <a class="menu-link @if(request()->path() === 'history') active @endif" href="/history">История</a>
        <a class="menu-link @if(request()->path() === 'projects') active @endif" href="/projects">Проекты</a>
    </div>
</div>
@yield('content')
</body>
</html>
