<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

final class File extends Model
{
    protected $table = 'files';

    protected $fillable = [
        'path',
        'name',
        'original_name',
        'extension',
        'mime',
        'size',
    ];

    public static function store(UploadedFile $file): self
    {
        $fullPath = $file->store('public/files');
        $explodedPath = explode('/', $fullPath);

        $file = new self([
            'path' => '/storage/files',
            'name' => array_pop($explodedPath),
            'original_name' => $file->getClientOriginalName(),
            'extension' => $file->extension(),
            'mime' => $file->getMimeType(),
            'size' => $file->getSize(),
        ]);

        $file->save();

        return $file;
    }

    public function delete(): void
    {
        unlink(storage_path(str_replace('/storage/', 'app/public/', $this->path) . '/' . $this->name));

        parent::delete();
    }
}
