<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

final class Project extends Model
{
    protected $table = 'projects';

    protected $fillable = [
        'title',
        'category',
        'description',
        'preview_id',
        'visible',
        'sortable_rank',
    ];

    public static function add(string $title, string $category, UploadedFile $uploadedFile, ?string $description): self
    {
        $project = new self([
            'title' => $title,
            'category' => $category,
            'description' => $description,
            'preview_id' => File::store($uploadedFile)->id,
            'visible' => false,
            'sortable_rank' => 0,
        ]);

        $project->save();

        return $project;
    }

    public function addImage(UploadedFile $uploadedFile): void
    {
        ProjectImage::add($this->id, $uploadedFile);
    }

    public function delete()
    {
        File::query()->find($this->preview_id)->delete();

        return parent::delete();
    }
}
