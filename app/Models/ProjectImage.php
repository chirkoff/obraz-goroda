<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

final class ProjectImage extends Model
{
    protected $table = 'project_images';

    protected $fillable = [
        'project_id',
        'file_id',
    ];

    public static function add(int $projectId, UploadedFile $uploadedFile): self
    {
        $file = File::store($uploadedFile);

        $projectImage = new self([
            'project_id' => $projectId,
            'file_id' => $file->id,
        ]);

        $projectImage->save();

        return $projectImage;
    }
}
