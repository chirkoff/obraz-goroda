<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

final class IndexController extends Controller
{
    public function __invoke(Request $request): View
    {
        return view('admin.index');
    }

    public function getProjectsForApiTables(Request $request): JsonResponse
    {
        $projectsCount = Project::query()->count();

        /** @var \PDOStatement */
        $pdoStatement = DB::connection()->getPdo()->prepare(
            <<<'SQL'
                SELECT
                       projects.*,
                       files.path as image_path, files.name as image_name
                FROM projects
                LEFT JOIN files ON files.id = projects.preview_id
                ORDER BY projects.sortable_rank
                LIMIT :limit OFFSET :offset
            SQL
        );

        $pdoStatement->execute([
            ':limit' => $request->query->getInt('length', 10),
            ':offset' => $request->query->getInt('start'),
        ]);

        $projects = $pdoStatement->fetchAll(\PDO::FETCH_ASSOC);

        return new JsonResponse([
            'draw' => $request->query->getInt('draw'),
            'recordsTotal' => $projectsCount,
            'recordsFiltered' => $projectsCount,
            'data' => $projects
        ]);
    }

    public function createProject(Request $request): RedirectResponse
    {
        Project::add(
            $request->get('title'),
            $request->get('category'),
            $request->file('preview'),
            $request->get('description'),
        );

        return redirect()->to('/admin/projects');
    }

    public function deleteProject(int $id)
    {
        Project::query()->find($id)->delete();

        return redirect()->to('/admin/projects');
    }

    public function projects()
    {
        return view('admin.projects');
    }

    public function customers()
    {
        return view('admin.customers');
    }
}
