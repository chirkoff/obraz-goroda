<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

final class ProjectController extends Controller
{
    public function index(): View
    {
        /** @var \PDOStatement */
        $pdoStatement = DB::connection()->getPdo()->prepare(
            <<<'SQL'
                SELECT
                    projects.*,
                    files.path as preview_path, files.name as preview_name
                FROM projects
                INNER JOIN files ON files.id = projects.preview_id
                ORDER BY projects.sortable_rank
            SQL
        );

        $pdoStatement->execute();

        $projects = $pdoStatement->fetchAll(\PDO::FETCH_ASSOC);

        return view('projects', ['projects' => $projects]);
    }
}
